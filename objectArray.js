let courses = [];


//Create:
console.log("");
console.log("Create Function Section:");

const postCourse = (id,name,description,price) => {
	courses.push({
		id: id,
		name: name,
		description: description,
		price: price,
		isActive: true
	})
	alert(`You have created ${name}. The price is ${price}.`)
}

console.log("Added a course.");
postCourse("TECWRIT1","Technical Writing 1","Class about writing formal or business letters.",15000);

console.log("Added a 2nd course.");
postCourse("PRGRM3","Programming 3"," Advanced class in porgramming methods.",23000);

console.log("Added a 3rd course.");
postCourse("SLFDBT5","Self-doubt 5","5th part about doubting oneself.",50000);

console.log("List of courses after \"postCourse\".");
console.log(courses);



//Retrieve:
console.log("");
console.log("");
console.log("Retrieve Function Section:");

const getSingleCourse = courseId => {
	console.log(`Searching for course with the ID: "${courseId}."`);

	let foundCourse = courses.find((courseName)=>{
		return courseName.id === courseId;
	})
	if (foundCourse !== undefined) {
		console.log("Course found! Here are the course's data:");
		console.log(foundCourse);
	} else {
		console.log("No course found.")
	}
}

getSingleCourse("TECWRIT1");
getSingleCourse("ALGEB-X");



//Delete:
console.log("");
console.log("");
console.log("Delete Function Section:");

const deleteCourse = () => {
	courses.pop();
	console.log("Last course in the list is removed.")
}

deleteCourse();
console.log("List of courses after \"deleteCourse\".");
console.log(courses);